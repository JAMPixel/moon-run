/**
 * 
 */

game.MenuScreen = me.ScreenObject.extend(
{
	onResetEvent: function()
	{
		me.game.world.addChild(new me.ColorLayer("background", game.cst.BACKGROUND_COLOR), 0);
		var starPos = game.util.generateStarPos(game.width, game.height, 30);
		for (var i = 0; i < starPos.length; ++i)
		{
			me.game.world.addChild(new game.StarEntity(starPos[i].x, starPos[i].y));
		}
		me.game.world.addChild(new me.Sprite(0.8*game.width, 0.3*game.height, 
			{
				image: "earth",
				framewidth: 128,
				frameheight: 128
			}
		));
	},

	onDestroyEvent: function()
	{
		
	}
});
