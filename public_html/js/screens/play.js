/**
 * 
 */

game.PlayScreen = me.ScreenObject.extend(
{
	onResetEvent: function()
	{
		me.game.world.addChild(new me.ColorLayer("background", game.cst.BACKGROUND_COLOR), 1);
		var starPos = game.util.generateStarPos(game.width, game.height, 30);
		for (var i = 0; i < starPos.length; ++i)
		{
			me.game.world.addChild(new game.StarEntity(starPos[i].x, starPos[i].y));
		}
		me.game.world.addChild(new me.Sprite(0.8*game.width, 0.3*game.height, 
			{
				image: "earth",
				framewidth: 128,
				frameheight: 128
			}
		));
		
		me.levelDirector.loadLevel("level1");
		
		//this.HUD = new game.HUD.Container();
		//me.game.world.addChild(this.HUD);
		
		this.player = me.game.world.addChild(new game.PlayerEntity(320, 320));
		
		me.input.registerPointerEvent('pointerdown', me.game.viewport, this.player.onMouseDownEvent.bind(this.player));
		me.input.registerPointerEvent('pointermove', me.game.viewport, this.player.onMouseMoveEvent.bind(this.player));
		me.input.registerPointerEvent('pointerup', me.game.viewport, this.player.onMouseUpEvent.bind(this.player));
	},

	onDestroyEvent: function()
	{
		me.input.releasePointerEvent('pointerdown', me.game.viewport);
		me.input.releasePointerEvent('pointermove', me.game.viewport);
		me.input.releasePointerEvent('pointerup', me.game.viewport);
	}
});


