/**
 * 
 */

var privateUtils = {
	
	minDistance2: 1000,
	
	newPos: function(positions, maxX, maxY)
	{
		var x, y;
		do
		{
			x = Math.floor(Math.random() * maxX);
			y = Math.floor(Math.random() * maxY);
		}
		while (!privateUtils.isValidPos(positions, x, y))
		
		return {
			x: x,
			y: y
		};
	},
	
	isValidPos: function(positions, x, y)
	{
		var isValid = true;
		for (var i = 0; i < positions.length; ++i)
		{
			if (Math.pow(positions[i].x - x, 2) + Math.pow(positions[i].y - y, 2) < privateUtils.minDistance2)
			{
				isValid = false;
			}
		}
		return isValid;
	}
};

game.util = {
	
	generateStarPos: function(maxX, maxY, n)
	{
		var positions = [];
		for (var i = 0; i < n; ++i)
		{
			positions.push(privateUtils.newPos(positions, maxX, maxY));
		}
		return positions;
	}
};
