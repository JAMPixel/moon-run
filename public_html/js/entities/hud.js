/** 
 * 
 */

game.HUD = game.HUD || {};

game.HUD.Container = me.Container.extend(
{
	init: function()
	{
		this._super(me.Container, "init");
		
		this.isPersistent = true;
		
		this.addChild(new game.HUD.FuelBar(10, 10));
		
		this.z = Infinity;
	}
});

game.HUD.FuelBar = me.GUI_Object.extend(
{
	init: function(x, y)
	{
		this._super(me.GUI_Object, "init", [x, y,
			{
				//image: "",
				//framewidth: ,
				//frameheight: 
			}
		]);
	}
});
