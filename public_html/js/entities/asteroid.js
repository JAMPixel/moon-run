/* 
 *
 */


game.AsteroidEntity = me.Entity.extend(
{
	init: function(x, y, v)
	{
		this._super(me.Entity, 'init', [x, y,
			{
				width: 48,
				height: 48,
				image: 'asteroid',
				framewidth: 48,
				frameheight: 48
			}
		]);
		
		this.renderable.addAnimation("idle", [0, 1, 2, 3], 150);
		this.renderable.setCurrentAnimation("idle");
		this.body.vel.x = v.x * game.cst.ASTEROID_SPEED;
		this.body.vel.y = v.y * game.cst.ASTEROID_SPEED;
	},
	
	update: function(dt)
	{
		this.body.update(dt);
		me.collision.check(this);
		
		return true;
	},
	
	onCollision: function(response, other)
	{
		me.game.world.addChild(new game.AsteroidSheet(this.pos.x, this.pos.y));
		me.game.world.removeChild(this);
	}
});

game.AsteroidSheet = me.AnimationSheet.extend(
{
	init: function(x, y)
	{
		this._super(me.AnimationSheet, 'init', [x, y, 
			{	
				image: 'destroy',
				framewidth: 48,
				frameheight: 48
			}
		]),

		this.addAnimation("destroy", [4, 5, 6, 7], 150);
		this.setCurrentAnimation("destroy", (function () {
			me.game.world.removeChild(this);
			return false;
		}).bind(this));			
	}
});