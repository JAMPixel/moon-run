/**
 * 
 */

game.PlayerEntity = me.Entity.extend(
{
	init: function(x, y)
	{
		this._super(me.Entity, 'init', [x, y,
			{
				name: 'player',
				width: 64,
				height: 96,
				image: 'player',
				framewidth: 64,
				frameheight: 96
			}
		]);
		
		this.mousePos = new me.Vector2d(0, 0);
		this.lastJetpackEffect = 0;
		
		this.renderable.addAnimation('idle_r',  [0]);
		this.renderable.addAnimation('walk_r',  [0, 1], 150);
		this.renderable.addAnimation('jump_r',  [1]);
		
		this.renderable.addAnimation('idle_l',  [2]);
		this.renderable.addAnimation('walk_l',  [2, 3], 150);
		this.renderable.addAnimation('jump_l',  [3]);
		
		this.onGround = false;
		this.setAnimation('jump_r');
		
		me.game.viewport.follow(this, me.game.viewport.AXIS.BOTH);
	},
	
	update: function(dt)
	{
		var horizDir = 0;
		var right = me.input.isKeyPressed('right');
		var left = me.input.isKeyPressed('left');
		
		if (right && !left)
		{
			horizDir = 1;
			if (this.anim.dir == 'l')
			{
				this.setAnimation(this.anim.type + '_r');
			}
		}
		if (left && !right)
		{
			horizDir = -1;
			if (this.anim.dir == 'r')
			{
				this.setAnimation(this.anim.type + '_l');
			}
		}
		
		if (horizDir == 0)
		{
			if (this.body.vel.x > 0)
				--horizDir;
			if (this.body.vel.x < 0)
				++horizDir;
		}
		
		if (this.onGround)
		{
			if (horizDir > 0 && this.anim.type != 'walk')
			{
				this.setAnimation('walk_r');
			}
			if (horizDir < 0 && this.anim.type != 'walk')
			{
				this.setAnimation('walk_l');
			}
			if (horizDir == 0 && this.anim.type != 'idle')
			{
				this.setAnimation('idle_' + this.anim.dir);
			}
		}
		
		this.body.accel.x = horizDir * game.cst.PLAYER_SPEED;
		this.body.vel.x += this.body.accel.x * me.timer.tick;
		
		this.body.accel.y = game.cst.GRAVITY;
		
		this.body.vel.y += this.body.accel.y * me.timer.tick;
		
		if (me.input.isKeyPressed('jump') && this.onGround)
		{
			this.body.vel.y = -game.cst.PLAYER_JUMP_SPEED;
			this.onGround = false;
			this.setAnimation('jump_' + this.anim.dir);
		}
		
		this.lastJetpackEffect += dt;
		if (this.mouseDown)
		{
			var v = me.game.viewport.worldToLocal(this.pos.x + this.width/2, this.pos.y + this.height/2);
			v.negateSelf().add(this.mousePos);
			if (v.length() > 20)
			{
				v.normalize().scale(game.cst.JETPACK_FORCE, game.cst.JETPACK_FORCE);
				
				this.body.vel.x += v.x * me.timer.tick;
				this.body.vel.y += v.y * me.timer.tick;
				if (this.lastJetpackEffect > 100)
				{
					this.lastJetpackEffect = 0;
					console.log("spawn jetpack effect");
					me.game.world.addChild(new game.JetpackEffect(this.pos.x, this.pos.y, this.anim.dir));
				}
			}
		}
		
		if (this.body.vel.x > game.cst.MAX_PLAYER_SPEED)
			this.body.vel.x = game.cst.MAX_PLAYER_SPEED;
		if (this.body.vel.x < -game.cst.MAX_PLAYER_SPEED)
			this.body.vel.x = -game.cst.MAX_PLAYER_SPEED;
		if (this.body.vel.y > game.cst.MAX_PLAYER_SPEED)
			this.body.vel.y = game.cst.MAX_PLAYER_SPEED;
		if (this.body.vel.y < -game.cst.MAX_PLAYER_SPEED)
			this.body.vel.y = -game.cst.MAX_PLAYER_SPEED;
		
		this.body.update(dt);
		me.collision.check(this);
		
		return true;
	},
	
	onCollision: function (response, other)
	{
		if (!this.onGround && response.overlapV.y > 0)
		{
			this.onGround = true;
			this.setAnimation('idle_' + this.anim.dir);
		}
		return true;
	},
	
	onMouseDownEvent: function(e)
	{
		this.mouseDown = true;	
		//me.game.world.addChild(new game.AsteroidEntity(e.gameWorldX, e.gameWorldY, new me.Vector2d(-1.414,1.414)));
	},
	
	onMouseMoveEvent: function(e)
	{
		this.mousePos.set(e.gameScreenX, e.gameScreenY);
	},
	
	onMouseUpEvent: function(e)
	{
		this.mouseDown = false;
	},
	
	setAnimation: function(name)
	{
		var splited = name.split('_');
		this.anim = {
			type: splited[0],
			dir: splited[1]
		};
		this.renderable.setCurrentAnimation(name);
	}
});
