/** 
 * 
 */

game.StarEntity = me.AnimationSheet.extend(
{
	init: function(x, y)
	{
		this._super(me.AnimationSheet, "init", [x, y ,
			{
				image: "star",
				framewidth: 18,
				frameheight: 18
			}
		]);
		
		this.addAnimation("idle", [0, 1, 2, 3, 2, 1], 150);
		this.setCurrentAnimation("idle");
		setTimeout(function()
		{
			this.setCurrentAnimation("idle");
		}
		.bind(this), Math.floor(Math.random() * 900));
	},
	
	update: function(dt)
	{
		return true;
	}
});
