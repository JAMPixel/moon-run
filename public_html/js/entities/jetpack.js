/** 
 * 
 */

game.JetpackEffect = me.AnimationSheet.extend(
{
	init: function(x, y, animDir)
	{
		this._super(me.AnimationSheet, "init", [x, y ,
			{
				image: "jetpack",
				framewidth: 64,
				frameheight: 96
			}
		]);
		
		this.addAnimation('r', [0, 1], 400);
		this.addAnimation('l', [2, 3], 400);
		this.setCurrentAnimation(animDir, function()
		{
			me.game.world.removeChild(this);
			return false;
		}
		.bind(this));
	}
});
