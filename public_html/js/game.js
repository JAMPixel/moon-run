/**
 * 
 */

me.state.INTRO = me.state.USER + 1;

var game = {
	
	cst: {
		BACKGROUND_COLOR: "rgba(27, 0, 50, 255)",
		GRAVITY: 0.00000001,
		PLAYER_SPEED: 1,
		MAX_PLAYER_SPEED: 8,
		PLAYER_JUMP_SPEED: 18,
		JETPACK_FORCE: 3,
		ASTEROID_SPEED: 5
	},
	
	resources: [
		{name: "star", type: "image", src: "data/img/star_18px.png"},
		{name: "earth", type: "image", src: "data/img/earth_128px.png"},
		{name: "asteroid", type: "image", src: "data/img/asteroid_48px.png"},
		{name: "slime_blue", type: "image", src: "data/img/slime_blue_64px.png"},
		{name: "slime_brown", type: "image", src: "data/img/slime_brown_64px.png"},
		{name: "slime_green", type: "image", src: "data/img/slime_green_64px.png"},
		{name: "player", type: "image", src: "data/img/player_64px.png"},
		{name: "jetpack", type: "image", src: "data/img/jetpack_64px.png"},
		{name: "tileset_img", type: "image", src: "data/img/tileset_64px.png"},
		
		{name: "tileset", type: "tsx", src: "data/map/tileset.tsx"},
		
		{name: "level1", type: "tmx", src: "data/map/level1.tmx"}
	],
	
	onload: function()
	{
		game.width = window.innerWidth;
		game.height = window.innerHeight;
		if (!me.video.init(game.width, game.height,
			{
				wrapper: "screen",
				scale: 'auto'
			}))
		{
			alert("Your browser does not support HTML5 canvas.");
			return;
		}
		
		me.audio.init("mp3,ogg");
		
		me.loader.onload = game.loaded;
		
		me.loader.preload(game.resources);
		
		me.state.change(me.state.LOADING);
	},
	
	loaded: function()
	{
		me.state.set(me.state.INTRO, new game.IntroScreen());	
		me.state.set(me.state.MENU, new game.MenuScreen());	
		me.state.set(me.state.PLAY, new game.PlayScreen());
		
		me.pool.register("wall", game.WallEntity);
		
		me.input.bindKey(me.input.KEY.LEFT, "left");
		me.input.bindKey(me.input.KEY.RIGHT, "right");
		me.input.bindKey(me.input.KEY.UP, "jump");
		
		me.state.change(me.state.INTRO);
	}
};
